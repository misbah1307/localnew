# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,get_object_or_404# Create your views here.
from django.http import HttpResponse,HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from .models import Question, Choice
from django.http import Http404
from django.views import generic
from django.core.mail import send_mail





def index(request):
	latest_question_list = Question.objects.order_by('-pub_date')[:5]
	context = {'latest_question_list': latest_question_list}
	return render(request,'polls/index.html',context)

def detail(request, question_id):
	try:
		question = Question.objects.get(id=question_id)
		context = {'que':'','id':''}
		context['que']=question
		context['id']=question_id
	except Question.DoesNotExist:
		raise Http404("Question Doest not exist")

	return render(request,'polls/details.html',context)

def results(request, question_id):
   question = get_object_or_404(Question, pk=question_id)
   return render(request, 'polls/results.html',{'question': question})

def vote(request, question_id):
	question = get_object_or_404(Question, pk=question_id)
	try:
		selected_choice = Choice.objects.get(id=request.POST['choice'])
	except (KeyError, Choice.DoesNotExist):
		return render(request,'polls/details.html',{'que':question,'error_message':"you didnt select any choice"})
	else:
		selected_choice.votes+=1
		selected_choice.save()
		return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

#email view
def mail(request):
	res = send_mail("subject for test","this is message for test","misbah1307@gmail.com",["misbah1307@gmail.com"],fail_silently=False)
	return HttpResponse('%s'%res)

#Generic views
#class IndexView(generic.ListView):
#    template_name = 'polls/index.html'
#    context_object_name = 'latest_question_list'

#    def get_queryset(self):
#        """Return the last five published questions."""
#        return Question.objects.order_by('-pub_date')[:5]


#class DetailView(generic.DetailView):
#    model = Question
#    template_name = 'polls/details.html'


#class ResultsView(generic.DetailView):
#    model = Question
#    template_name = 'polls/results.html'